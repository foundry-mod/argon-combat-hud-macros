
Hooks.once("init", function () {
    /*game.settings.register("echmacro", "echautoenabler", {
        name: "Auto ECH Enabler",
        hint: "Automatically activate Extended Combat HUD on startup",
        scope: "world",
        config: true,
        type: Boolean,
        default: true,
    });*/

    game.settings.register("echmacro", "macronumber", {
      name: "Number of macro slots",
      scope: "client",
      config: true,
      range: {
        min: 2,
        max: 10,
        step: 2,
      },
      type: Number,
      default: 4,
      onChange: () => {
          if(ui.ARGON.rendered) canvas.hud.echmacros.bind(ui.ARGON._token)
      },
    });
});