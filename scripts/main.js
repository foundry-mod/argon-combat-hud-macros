Hooks.once("ready", () => {
	if (
		game.modules.get(CONSTANTS.ENHANCED_COMBAT_HUD)?.active
	) {
		console.log(`EHC - Macros | ${CONSTANTS.ENHANCED_COMBAT_HUD} found.`);
	}
});

Hooks.once("init", () => {
	Hooks.on("renderHeadsUpDisplay", async (app, html, data) => {
	  canvas.hud.echmacros = new CombatHudMacro()
	})

	Hooks.on("renderArgonComponentArgonComponent", () => {
		setTimeout(async () => {
			canvas.hud.echmacros.options.parent = $(document).find('.extended-combat-hud .action-hud')
			canvas.hud.echmacros.bind(ui.ARGON._token)
		})
	})
});

$(window).on("drop", function(event) {
	let data;
	try {
	  data = JSON.parse(event.originalEvent.dataTransfer.getData("text/plain"));
	} catch (err) {
	  //return false;
	}
	if(data.slot !== undefined) canvas.hud.echmacros.dragDropMacro(Number(data.slot))
})

class CombatHudMacro extends BasePlaceableHUD {
	static get defaultOptions() {
	  const options = super.defaultOptions
	  options.template =
		"modules/echmacro/templates/extendedCombatHudMacro.html"
	  options.id = "echmacros"
	  options.dragDrop = [{ dragSelector: null, dropSelector: null }]
	  return options
	}
	
	_injectHTML(html) {
		if(this.options.parent.length) {
			const parent = $(this.options.parent).find('.actions-container[data-passcont]')
			if(parent.length) parent.before(html)
			else $(this.options.parent).append(html)
		} else $("body").append(html)
		this._element = html
		html.hide().fadeIn(200)
	}

	setPosition() {}

	_canDragDrop(selector) {
	  return true;
	}
	
    _contextMenu(html) {
		let contextMenu = ContextMenu.create(this,
			html,
			".macro-element",
			[
				{
				  name: "MACRO.Edit",
				  icon: '<i class="fas fa-edit"></i>',
				  condition: li => {
					const macro = game.macros.get(li.data("macro-id"));
					return macro ? macro.isOwner : false;
				  },
				  callback: li => {
					const macro = game.macros.get(li.data("macro-id"));
					macro.sheet.render(true);
				  }
				},
				{
				  name: "MACRO.Remove",
				  icon: '<i class="fas fa-times"></i>',
				  condition: li => !!li.data("macro-id"),
				  callback: li => this.dragDropMacro(Number(li.data("slot")))
				},
				{
				  name: "MACRO.Delete",
				  icon: '<i class="fas fa-trash"></i>',
				  condition: li => {
					const macro = game.macros.get(li.data("macro-id"));
					return macro ? macro.isOwner : false;
				  },
				  callback: li => {
					const macro = game.macros.get(li.data("macro-id"));
					return Dialog.confirm({
					  title: `${game.i18n.localize("MACRO.Delete")} ${macro.name}`,
					  content: `<h4>${game.i18n.localize("AreYouSure")}</h4><p>${game.i18n.localize("MACRO.DeleteWarning")}</p>`,
					  yes: async () => {
						await this.dragDropMacro(Number(li.data("slot")))
						await macro.delete.bind(macro)();
					  }
					});
				  }
				}
			]
		)

		contextMenu._setPosition = function(html, target) {	
			let scale = true//game.settings.get("enhancedcombathud", "noAutoscale")
			? game.settings.get("enhancedcombathud", "scale")
			: (1 / (echHUDWidth / windowWidth)) *
				game.settings.get("enhancedcombathud", "scale");	
			// Append to target and get the context bounds
			target.css("position", "relative");
			html.css("visibility", "hidden")
			if(scale < 1) {
				html.css("transform-origin","bottom left")
				.css("transform",`scale(${1/scale})`)
			}
			target.append(html);
		
			// Display the menu
			html.addClass("expand-up");
			html.css("visibility", "");
			target.addClass("context");
		}
	}

	async _onDrop(event) {
		event.preventDefault();  
		event.stopPropagation();
		
		console.log(event)

		let slot = event.target?.dataset?.slot;
		let data;

		try {
		  data = JSON.parse(event.dataTransfer.getData("text/plain"));
		} catch (err) {
		  //return false;
		}

		if (slot && data) {
     	  const macro = await fromUuid(data.uuid),
		  	_this = ui.ARGON;
		  if(slot !== undefined && data.slot !== undefined && slot === data.slot) return;
		  if(data.slot !== undefined) {
			if(event.target.dataset.macroId) {
				let oldMacro = game.users.current.getFlag('echmacro',_this._actor._id).find(e => e.slot === Number(slot) )
				if(oldMacro) await this.dragDropMacro(Number(data.slot), oldMacro.macro, false)
			} else await this.dragDropMacro(Number(data.slot), null, false)
		  }
		  if((macro instanceof Macro || macro instanceof Item || macro instanceof Item5e) && macro.isOwner) await this.dragDropMacro(Number(slot), {id: macro.id, name: macro.name, img: macro.img, type: (macro instanceof Macro ? 'macro' : 'item')});
		}
	}

	async getData() {
	  const data = super.getData();
	  data.macros = this.getUserMacros();
	  data.count = game.settings.get("echmacro", "macronumber");
	  return data;
	}

	close() {
	  super.close();
	}
  
	activateListeners(html) {
	  super.activateListeners(html);
	  let macroElements = $(html).find(".macro-element")
	  $(html).find(".macro-element:not([data-macro-id=''])").attr("draggable", true)
	  $(macroElements)
	  .on("dragstart", async (event) => {
		event.originalEvent.dataTransfer.setData(
			"text",
			JSON.stringify({slot: event.currentTarget.dataset.slot, uuid: `Macro.${event.currentTarget.dataset.macroId}`})
		);
	  })
	  $(html).find(".macro-container-before").on("click", () => ui.macros.renderPopout(true))
	  this.setMacroClick(macroElements)
	  this._contextMenu(html);
	}

	getUserMacros() {
		let _this = ui.ARGON,
			count = game.settings.get("echmacro", "macronumber"),
			macros = Array(count).fill(null);
	
		let userMacros = game.users.current.getFlag('echmacro',_this._actor._id)
		if(!userMacros) return macros;
		
		userMacros.forEach((e) => {
		  if(e && e.slot < count) macros[e.slot] = e.macro;
		});
		return macros
	}

	setMacroClick(target) {
		let _this = ui.ARGON
		target
		.on("click", async (event) => {
		  let macroId = event.currentTarget.dataset.macroId;
		  if(macroId) {
			if(event.currentTarget.dataset.type === 'macro') {
				const macro = game.macros.get(macroId);
				if(macro) macro.execute(_this._token ?? _this._actor);
			} else {
				const macro = _this._actor.items.get(macroId)
				console.log(macro)
				if(macro) macro.use()
			}
		  } else {
			const macro = await Macro.create({name: "New Macro", type: "chat", scope: "global"});
			await this.dragDropMacro(Number(event.currentTarget.dataset.slot), {id: macro.id, name: macro.name, img: macro.img, type: 'macro'})
			macro.sheet.render(true);
		  }
		})
	}

	async dragDropMacro(slot, macro = null, toRender = true) {
		let _this = ui.ARGON,
			actorMacros = game.users.current.getFlag('echmacro',_this._actor._id);
		if(!actorMacros) actorMacros = [];
		actorMacros = actorMacros.filter(e => e.slot !== slot)
		if(macro) actorMacros.push({slot: slot, macro: macro});
		await game.users.current.setFlag('echmacro',_this._actor._id,actorMacros)
		if(toRender) this.render(true)
	}
}